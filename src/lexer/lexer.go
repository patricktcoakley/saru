package lexer

import "saru/src/token"

type Lexer struct {
	input   string
	pos     int
	readPos int
	char    byte
}

func NewLexer(in string) *Lexer {
	l := &Lexer{input: in}
	l.readChar()
	return l
}

func (l *Lexer) NextToken() token.Token {
	var t token.Token

	l.skipWhitespace()

	switch l.char {

	case '=':
		if l.peekChar() == '=' {
			char := l.char
			l.readChar()
			literal := string(char) + string(l.char)
			t = token.Token{TokenType: token.Equals, Literal: literal}
		} else {
			t = token.NewToken(token.Assign, l.char)
		}
	case '!':
		if l.peekChar() == '=' {
			char := l.char
			l.readChar()
			literal := string(char) + string(l.char)
			t = token.Token{TokenType: token.NotEquals, Literal: literal}
		} else {
			t = token.NewToken(token.Bang, l.char)
		}
	case ';':
		t = token.NewToken(token.SemiColon, l.char)
	case '(':
		t = token.NewToken(token.LParen, l.char)
	case ')':
		t = token.NewToken(token.RParen, l.char)
	case ',':
		t = token.NewToken(token.Comma, l.char)
	case '+':
		t = token.NewToken(token.Plus, l.char)
	case '-':
		t = token.NewToken(token.Minus, l.char)
	case '/':
		t = token.NewToken(token.Slash, l.char)
	case '*':
		t = token.NewToken(token.Asterisk, l.char)
	case '<':
		t = token.NewToken(token.LBracket, l.char)
	case '>':
		t = token.NewToken(token.RBracket, l.char)
	case '{':
		t = token.NewToken(token.LBrace, l.char)
	case '}':
		t = token.NewToken(token.RBrace, l.char)
	case 0:
		t.TokenType = token.Eof
		t.Literal = ""
	default:
		if isLetter(l.char) {
			t.Literal = l.readId()
			t.TokenType = token.LookupId(t.Literal)
			return t
		} else if isDigit(l.char) {
			t.TokenType = token.Int
			t.Literal = l.readNum()
			return t
		} else {
			t = token.NewToken(token.Illegal, l.char)
		}
	}

	l.readChar()

	return t

}

func (l *Lexer) readId() string {
	pos := l.pos

	for isLetter(l.char) {
		l.readChar()
	}
	return l.input[pos:l.pos]
}

func isLetter(char byte) bool {
	return ('a' <= char && char <= 'z') || ('A' <= char && char <= 'Z') || char == '_' || char == '?' || char == '!' || char == '-'
}

func isDigit(char byte) bool {
	return '0' <= char && char <= '9'
}

func (l *Lexer) skipWhitespace() {
	for l.char == ' ' || l.char == '\t' || l.char == '\n' || l.char == '\r' {
		l.readChar()
	}
}

func (l *Lexer) readNum() string {
	pos := l.pos
	for isDigit(l.char) {
		l.readChar()
	}

	return l.input[pos:l.pos]
}

func (l *Lexer) peekChar() byte {
	if l.readPos >= len(l.input) {
		return 0
	}

	return l.input[l.readPos]
}

func (l *Lexer) readChar() {
	if l.readPos >= len(l.input) {
		l.char = 0
	} else {
		l.char = l.input[l.readPos]
	}

	l.pos = l.readPos
	l.readPos += 1
}
