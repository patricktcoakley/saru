package repl

import (
	"bufio"
	"fmt"
	"io"
	"saru/src/lexer"
	"saru/src/token"
)

const Prompt = "=> "

func Run(in io.Reader) {
	scanner := bufio.NewScanner(in)

	for {
		fmt.Print(Prompt)
		scanned := scanner.Scan()

		if !scanned {
			return
		}

		line := scanner.Text()
		l := lexer.NewLexer(line)

		for t := l.NextToken(); t.TokenType != token.Eof; t = l.NextToken() {
			fmt.Printf("%+v\n", t)
		}
	}
}
