package lexer

import (
	"saru/src/token"
	"testing"
)

func TestNextToken(t *testing.T) {
	input := `let five = 5;
let ten = 10;

let add = fun(x,y) {
	x + y;
};

let result = add(five, ten);
!-/*5;
5 < 10 > 5;

if (5 < 10) {
	return true;
} else {
	return false;
}

10 == 10;
10 != 9;
`

	tests := []struct {
		expectedType    token.Type
		expectedLiteral string
	}{
		{token.Let, "let"},
		{token.Id, "five"},
		{token.Assign, "="},
		{token.Int, "5"},
		{token.SemiColon, ";"},

		{token.Let, "let"},
		{token.Id, "ten"},
		{token.Assign, "="},
		{token.Int, "10"},
		{token.SemiColon, ";"},

		{token.Let, "let"},
		{token.Id, "add"},
		{token.Assign, "="},
		{token.Function, "fun"},
		{token.LParen, "("},
		{token.Id, "x"},
		{token.Comma, ","},
		{token.Id, "y"},
		{token.RParen, ")"},
		{token.LBrace, "{"},
		{token.Id, "x"},
		{token.Plus, "+"},
		{token.Id, "y"},
		{token.SemiColon, ";"},
		{token.RBrace, "}"},
		{token.SemiColon, ";"},

		{token.Let, "let"},
		{token.Id, "result"},
		{token.Assign, "="},
		{token.Id, "add"},
		{token.LParen, "("},
		{token.Id, "five"},
		{token.Comma, ","},
		{token.Id, "ten"},
		{token.RParen, ")"},
		{token.SemiColon, ";"},

		{token.Bang, "!"},
		{token.Minus, "-"},
		{token.Slash, "/"},
		{token.Asterisk, "*"},
		{token.Int, "5"},
		{token.SemiColon, ";"},

		{token.Int, "5"},
		{token.LBracket, "<"},
		{token.Int, "10"},
		{token.RBracket, ">"},
		{token.Int, "5"},
		{token.SemiColon, ";"},

		{token.If, "if"},
		{token.LParen, "("},
		{token.Int, "5"},
		{token.LBracket, "<"},
		{token.Int, "10"},
		{token.RParen, ")"},
		{token.LBrace, "{"},

		{token.Return, "return"},
		{token.True, "true"},
		{token.SemiColon, ";"},

		{token.RBrace, "}"},
		{token.Else, "else"},
		{token.LBrace, "{"},

		{token.Return, "return"},
		{token.False, "false"},
		{token.SemiColon, ";"},
		{token.RBrace, "}"},

		{token.Int, "10"},
		{token.Equals, "=="},
		{token.Int, "10"},
		{token.SemiColon, ";"},

		{token.Int, "10"},
		{token.NotEquals, "!="},
		{token.Int, "9"},
		{token.SemiColon, ";"},

		{token.Eof, ""},
	}

	lexer := NewLexer(input)

	for i, tokenType := range tests {
		curToken := lexer.NextToken()

		if curToken.TokenType != tokenType.expectedType {
			t.Fatalf("tests[%d] - token type is wrong. expected=%q, got=%q", i, tokenType.expectedType, curToken.TokenType)
		}

		if curToken.Literal != tokenType.expectedLiteral {
			t.Fatalf("tests[%d] - literal is wrong. expected=%q, got=%q", i, tokenType.expectedLiteral, curToken.Literal)
		}
	}

}
