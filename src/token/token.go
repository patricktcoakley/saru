package token

const (
	Illegal   = "ILLEGAL"
	Eof       = "EOF"
	Id        = "ID"
	Int       = "INT"
	Assign    = "="
	Plus      = "+"
	Minus     = "-"
	Bang      = "!"
	Asterisk  = "*"
	Slash     = "/"
	LBracket  = "<"
	RBracket  = ">"
	Comma     = ","
	SemiColon = ";"
	LParen    = "("
	RParen    = ")"
	LBrace    = "{"
	RBrace    = "}"
	Function  = "FUNCTION"
	Let       = "LET"
	True      = "TRUE"
	False     = "FALSE"
	Return    = "RETURN"
	If        = "IF"
	Else      = "ELSE"
	Equals    = "=="
	NotEquals = "!="
)

type Type string

type Token struct {
	TokenType Type
	Literal   string
}

var keywords = map[string]Type{
	"fun":    Function,
	"let":    Let,
	"true":   True,
	"false":  False,
	"if":     If,
	"else":   Else,
	"return": Return,
}

func NewToken(tokenType Type, char byte) Token {
	return Token{TokenType: tokenType, Literal: string(char)}
}

func LookupId(id string) Type {
	if t, ok := keywords[id]; ok {
		return t
	}
	return Id
}
