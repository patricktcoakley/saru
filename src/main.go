package main

import (
	"fmt"
	"os"
	"saru/src/repl"
)

func main() {
	fmt.Printf("SARU\n\n")
	repl.Run(os.Stdin)
}
